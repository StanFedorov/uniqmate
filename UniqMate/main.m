//
//  main.m
//  UniqMate
//
//  Created by Stanislav Fedorov on 7/7/17.
//  Copyright © 2017 Stanislav Fedorov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

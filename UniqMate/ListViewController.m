//
//  ListViewController.m
//  UniqMate
//
//  Created by Stanislav Fedorov on 7/7/17.
//  Copyright © 2017 Stanislav Fedorov. All rights reserved.
//

#import "ListViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"

@interface ListViewController ()
    @property (strong, nonatomic) IBOutlet UIButton *people;
    @property (strong, nonatomic) IBOutlet UIButton *events;
    @property (strong, nonatomic) IBOutlet UILabel *city;
    @property (strong, nonatomic) IBOutlet UILabel *header;
    @property (strong, nonatomic) IBOutlet UILabel *interes1;
    @property (strong, nonatomic) IBOutlet UILabel *interes2;
    @property (strong, nonatomic) IBOutlet UILabel *interes3;
    @property (strong, nonatomic) IBOutlet UITableView *table;
    @property (nonatomic) int gridMode;
    @end

@implementation ListViewController
    
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.header setFont:[UIFont fontWithName:@"Lato-Bold" size:18]];
    [self.city setFont:[UIFont fontWithName:@"Lato-Bold" size:16]];
    [self.people.titleLabel setFont:[UIFont fontWithName:@"Lato-Heavy" size:15]];
    [self.events.titleLabel setFont:[UIFont fontWithName:@"Lato-Heavy" size:15]];
    self.table.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.interes1 setFont:[UIFont fontWithName:@"Lato-Heavy" size:13]];
    [self.interes2 setFont:[UIFont fontWithName:@"Lato-Heavy" size:13]];
    [self.interes3 setFont:[UIFont fontWithName:@"Lato-Heavy" size:13]];

    // Do any additional setup after loading the view.
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.gridMode == 0) {
        UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"personCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UIImageView *avatar = [cell viewWithTag:1];
        UILabel *name = [cell viewWithTag:2];
        UIButton *chat = [cell viewWithTag:3];
        UILabel *desc = [cell viewWithTag:4];
        
        UILabel *userInteres1 = [cell viewWithTag:5];
        UILabel *userInteres2 = [cell viewWithTag:6];
        [userInteres1 setFont:[UIFont fontWithName:@"Lato-Heavy" size:11]];
        [userInteres2 setFont:[UIFont fontWithName:@"Lato-Heavy" size:11]];
        
        [name setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [desc setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [chat.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [avatar.layer setCornerRadius:38.0f];
        [avatar sd_setImageWithURL:[NSURL URLWithString:@"https://graph.facebook.com/100007474780280/picture?width=300"]];
        return cell;
    }else {
        UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"eventCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UILabel *title = [cell viewWithTag:1];
        UILabel *date = [cell viewWithTag:2];
        UILabel *place = [cell viewWithTag:3];
        UIButton *join = [cell viewWithTag:4];

        [title setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [date setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [place setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [join.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:17]];


        return cell;
    }
}
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.gridMode == 0)
        return 103;
    else
        return 203;
}
    
- (IBAction)peopleClicked:(id)sender {
    [self.people setBackgroundImage:[UIImage imageNamed:@"slider.png"] forState:UIControlStateNormal];
    [self.events setBackgroundImage:nil forState:UIControlStateNormal];
    self.gridMode = 0;
    [self.events setTitleColor:[UIColor colorWithRed:39/255.0f green:193/255.0f blue:255/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [self.people setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.table reloadData];
}
    
- (IBAction)eventsClicked:(id)sender {
    [self.events setBackgroundImage:[UIImage imageNamed:@"slider.png"] forState:UIControlStateNormal];
    [self.people setBackgroundImage:nil forState:UIControlStateNormal];
    self.gridMode = 1;
    [self.people setTitleColor:[UIColor colorWithRed:39/255.0f green:193/255.0f blue:255/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [self.events setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.table reloadData];
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
- (BOOL) prefersStatusBarHidden
    {
        return YES;
    }
    
    
    @end

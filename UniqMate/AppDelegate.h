//
//  AppDelegate.h
//  UniqMate
//
//  Created by Stanislav Fedorov on 7/7/17.
//  Copyright © 2017 Stanislav Fedorov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


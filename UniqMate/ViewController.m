//
//  ViewController.m
//  UniqMate
//
//  Created by Stanislav Fedorov on 7/7/17.
//  Copyright © 2017 Stanislav Fedorov. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <AFNetworking.h>
#import <SVProgressHUD.h>

@interface ViewController ()
    @property (strong, nonatomic) NSMutableDictionary *interestsModel;
    @property (strong, nonatomic)  NSArray *needs;
    @property (nonatomic) int needUploaded;
    @property (nonatomic) int interesUploaded;
    @property (strong, nonatomic) IBOutlet UIButton *facebook;
    @property (strong, nonatomic) IBOutlet UILabel *motto;
    @end

@implementation ViewController
    
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.motto setFont:[UIFont fontWithName:@"Lato-Medium" size:17]];
    [self.facebook setFont:[UIFont fontWithName:@"Lato-Bold" size:19]];
}
    
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if([userDefaults boolForKey:@"isLogged"]) {
        NSLog(@"YES");
        [self performSegueWithIdentifier: @"list" sender: self];
    }else {
        NSLog(@"NO");
    }
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
    
- (IBAction)facebookAuthButtonTapped:(id)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile",@"user_likes",@"user_posts",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             [SVProgressHUD showWithStatus:@"Sending data..."];
             NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
             [parameters setValue:@"id,name,email" forKey:@"fields"];
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
              startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                           id result, NSError *error) {
                  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                  [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
                  NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                  [params setObject:result[@"email"] forKey:@"email"];
                  [params setObject:result[@"name"] forKey:@"name"];
                  [params setObject:result[@"id"] forKey:@"login"];
                  [manager POST:@"http://cc23280.tmweb.ru/uniqmate/fbLogin.php" parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject)
                   {
                       NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                       [userDefaults setObject:[responseObject objectForKey:@"userId"] forKey:@"userId"];
                       [userDefaults setBool:YES forKey:@"isLogged"];
                       [userDefaults synchronize];
                       if([responseObject[@"dataCollected"] intValue] == 0)
                       [self checkPosts];
                       else {
                           [SVProgressHUD dismiss];
                           [self performSegueWithIdentifier: @"list" sender: self];
                       }
                   } failure:^(NSURLSessionTask *operation, NSError *error)
                   {
                       [SVProgressHUD dismiss];
                   }];
              }];
         }
     }];
}
    
    
- (void)checkLikes {
    self.interestsModel = [[NSMutableDictionary alloc] init];
    [[[FBSDKGraphRequest alloc]
      initWithGraphPath:@"me/likes"
      parameters:[[NSDictionary alloc] initWithObjectsAndKeys:@"name,category,category_list",@"fields", nil]]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             [self analyzeInterests:result[@"data"]];
         }
     }];
}
    
- (void)checkPosts {
    [[[FBSDKGraphRequest alloc]
      initWithGraphPath:@"me/posts"
      parameters:[[NSDictionary alloc] initWithObjectsAndKeys:@"message",@"fields", nil]
      HTTPMethod:@"GET"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             [self analyzePosts:result[@"data"]];
         }
     }];
}
    
    
- (void)analyzePosts:(NSArray*)posts {
    NSString *textToAnalyze = @"";
    for(NSDictionary *post in posts) {
        if(post[@"message"] != nil)
        textToAnalyze = [NSString stringWithFormat:@"%@\n%@",textToAnalyze,post[@"message"]];
    }
    [self watsonUpload:textToAnalyze];
}
    
    
- (void)analyzeInterests:(NSArray*)pages {
    for(NSDictionary *page in pages) {
        NSString *category = page[@"category"];
        if([category length] > 0){
            NSString *categoryUpd = page[@"category"];
            if([category containsString:@"Company"])
            categoryUpd = [categoryUpd stringByReplacingOccurrencesOfString:@"Company" withString:@""];
            if([category containsString:@"Local"])
            categoryUpd = [categoryUpd stringByReplacingOccurrencesOfString:@"Local" withString:@""];
            if([category containsString:@"&"])
            categoryUpd = [categoryUpd stringByReplacingOccurrencesOfString:@"&" withString:@""];
            if([category containsString:@"Website"])
            categoryUpd = [categoryUpd stringByReplacingOccurrencesOfString:@"Website" withString:@""];
            if([category containsString:@"Computer"] && ![category containsString:@"Computers"])
            categoryUpd = [categoryUpd stringByReplacingOccurrencesOfString:@"Computer" withString:@"Computers"];
            if([category containsString:@"Technology"])
            categoryUpd = [categoryUpd stringByReplacingOccurrencesOfString:@"Technology" withString:@"Technologies"];
            
            categoryUpd = [categoryUpd stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if([self.interestsModel objectForKey:category] == nil) {
                [self.interestsModel setObject:[NSNumber numberWithInteger:1] forKey:categoryUpd];
            }else {
                NSNumber *interesWeight = [self.interestsModel objectForKey:categoryUpd];
                int weight = [interesWeight intValue];
                interesWeight = [NSNumber numberWithInt:weight + 1];
                [self.interestsModel setObject:interesWeight forKey:categoryUpd];
            }
        }
    }
    self.interesUploaded = 0;
    NSString *key = [[self.interestsModel allKeys] objectAtIndex:self.interesUploaded];
    [self uploadInteres:key andPercent:[self.interestsModel objectForKey:key]];
}
        
- (void)uploadInteres:(NSString*)category andPercent:(NSNumber*)percent {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[userDefaults objectForKey:@"userId"] forKey:@"userId"];
    [params setObject:category  forKey:@"categoryId"];
    [params setObject:percent forKey:@"value"];
    [manager POST:@"http://cc23280.tmweb.ru/uniqmate/addInteres.php" parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         self.interesUploaded++;
         if(self.interesUploaded == [self.interestsModel allKeys].count-1) {
             [SVProgressHUD dismiss];
             [self performSegueWithIdentifier: @"list" sender: self];
         }else {
             NSString *key = [[self.interestsModel allKeys] objectAtIndex:self.interesUploaded];
             [self uploadInteres:key andPercent:[self.interestsModel objectForKey:key]];
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}
    
- (void)uploadNeed:(NSDictionary*)need {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[userDefaults objectForKey:@"userId"] forKey:@"userId"];
    [params setObject:need[@"trait_id"] forKey:@"trait_id"];
    [params setObject:need[@"percentile"] forKey:@"percent"];
    [manager POST:@"http://cc23280.tmweb.ru/uniqmate/addNeeds.php" parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         self.needUploaded++;
         if(self.needUploaded == self.needs.count-1) {
             [self checkLikes];
         }else {
             [self uploadNeed:[self.needs objectAtIndex:self.needUploaded]];
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}
    
- (void)watsonUpload:(NSString*)posts {
    NSData *postData = [[NSString stringWithFormat:@"%@",posts] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"POST"];
    [request setURL:[NSURL URLWithString:@"https://gateway.watsonplatform.net/personality-insights/api/v3/profile?version=2017-07-07"]];
    [request setValue:@"text/plain; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"en" forHTTPHeaderField:@"Content-Language"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", @"6d52a2ea-c357-42dd-bb42-bd33c13858b5", @"XjTSCFIAqLaW"];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
                           completionHandler: ^(NSURLResponse * response, NSData * data, NSError * error) {
                               NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse*)response;
                               NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                    options:kNilOptions
                                                                                      error:&error];
                               NSLog(@"json %@",json);
                               self.needs = [json objectForKey:@"needs"];
                               self.needUploaded = 0;
                               [self uploadNeed:[self.needs objectAtIndex:self.needUploaded]];
                           }
     ];
    
    
    
    
}
    
- (BOOL) prefersStatusBarHidden
    {
        return YES;
    }
    
    @end
